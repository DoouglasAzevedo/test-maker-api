const Test = require('../model/test.model')
const repoQuestion = require('../repository/question.repository')
const db = require('../db')

exports.findAll = async () => {
    let con = await db.connect()
    let result = await Test.find()
    
    con.disconnect()
    return result
}

 exports.save = async (data) => {
    let con = await db.connect()
    
    let newTest = new Test(data)
    newTest.creationDate = new Date();

    await newTest.save()
    con.disconnect()
}

exports.correction = async (test) => {
    let questions = await repoQuestion.findByTest(test.test)
    
    return questions.answers.map(a => {
        if (a.answers.description == test.answers.description) {
            test.answers.correct = True
        }
    })
}