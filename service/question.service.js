const repository = require('../repository/question.repository')
const ValidationException = require('../exception/validation.exception')

exports.save = async (question) => {
    try {
        
        if (question == null) {
            throw new ValidationException("Pergunta é obrigatório")
        }

        if (question.description == null || question.description === "") {
            throw new ValidationException("Descrição é obrigatório")
        }

        if (question.test === "" ) {
            throw new ValidationException("Teste é obrigatório")
        }

        if (question.answers.size === 0) {
            throw new ValidationException("As respostas são obrigatórias")
        }

        if (question.answers.description === "") {
            throw new ValidationException("Descrição da resposta é obrigatórias")
        }

        await repository.save(question)
    } catch (error) {
        console.log(error.message)
    }
}

function mapAnswer(answer) {
    return {
        _id: answer._id,
        description: answer.description
    }
}

function mapQuestion(question) {
    return {
        _id: question._id,
        description: question.description,
        answers: question.answers.map(mapAnswer)
    }
}

exports.findByTest = async (test) => {

    if (test == null) {
        throw new ValidationException("Teste é obrigatório para consulta")
    }

    let result = await repository.findByTest(test);
    return result.map(mapQuestion)
}