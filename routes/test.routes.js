const service = require('../service/test.service')

module.exports = (app) => {

    app.get('/test', async (req, res) => {
        let result = await service.findAll()
        res.json(result)
    })

    app.post('/test', async (req, res) => {
        await service.save(req.body)
        res.end()
    })

    app.post('/test/grade', async (req, res) => {
        try {
            let result = await service.testCorrection(req.body)
            res.json(result)
            res.end()
        } catch (error) {
            console.log(error.message)   
            res.end()
        }
    })

}