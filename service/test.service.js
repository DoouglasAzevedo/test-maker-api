const repository = require('../repository/test.repository')
const ValidationException = require('../exception/validation.exception')

exports.findAll = async () => {
    return await repository.findAll()
}

exports.save = async (test) => {
    try {
        
        if (test == null) {
            throw new ValidationException("Teste é obrigatório")
        }

        if (test.name == null || test.description === "") {
            throw new ValidationException("Descrição é obrigatório")
        }
    
        await repository.save(test)
    } catch (error) {
        console.log(error.message)
    }
}

exports.testCorrection = async (test) => {
    return await repository.correction(test)
}


